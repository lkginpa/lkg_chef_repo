#
# Cookbook:: motd-attributes
# Recipe:: default
#
# Copyright:: 2017, The Authors, All Rights Reserved.

node.default['motd-attributes']['company'] = "Chef"
node.default['motd-attributes']['message'] = "It's a wonderful day in the neighborhood!"

template "/etc/motd" do
  source "motd.erb"
  mode "0644"
end

  
